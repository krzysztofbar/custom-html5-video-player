// Elements
var player = document.querySelector('.player');
var video = player.querySelector('.viewer');
var progress = player.querySelector('.progress');
var progressBar = player.querySelector('.progress__filled');
var toggle = player.querySelector('.toggle');
var skipButtons = player.querySelectorAll('[data-skip]');
var ranges = player.querySelector('.player__slider');

//Functinos
function togglePlay() {
    var method = video.paused ? 'play' : 'pause';
    video[method]();
}

function updatButton() {
    var icon = this.paused ? 'Play' : 'Pause';
    toggle.textContent = icon;
}

function skip() {
    video.currentTime += parseFloat(this.dataset.skip);
}

function handleRangeUpdate() {
    video[this.name] = this.value;
}

//Listeners
video.addEventListener('click', togglePlay);
video.addEventListener('play', updatButton);
video.addEventListener('pause', updatButton);

toggle.addEventListener('click', togglePlay);
skipButtons.forEach(button => button.addEventListener('click', skip));
ranges.addEventListener('change', handleRangeUpdate);
ranges.addEventListener('mousemove', handleRangeUpdate);